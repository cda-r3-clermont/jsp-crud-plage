<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 05/09/2023
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Les plages de la côte !</title>
    <%@include file="parts/script-style.jsp"%>
</head>
<body>
<%@include file="parts/header.jsp"%>
<div class="container">
    <%@include file="parts/menu.jsp"%>
    <h1>Les plages de la côte méditerranéenne !</h1>
<c:if test="${plages.size() == 0}">
    <div class="text-center text-danger">
        <h1>Aucune plage à afficher !</h1>
    </div>
</c:if>
    <c:if test="${plages.size() != 0}">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Adresse</th>
                <th scope="col">Ville</th>
                <th scope="col">Image</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${plages}" var="plage">
                <tr>
                    <td>${plage.getNom()}</td>
                    <td>${plage.getNumber()} ${plage.getStreet()} ${plage.getCodePostal()}</td>
                    <td>${plage.getCity()}</td>
                    <td><img style="max-height:100px" class="img-thumbnail" src="${plage.getImage()}"></td>
                    <td>
                        <div>
                            <a href="detail?idPlage=${plage.getId()}">
                                Voir la plage
                            </a>
                        </div>

                        <div>
                            <a href="delete?idPlage=${plage.getId()}">
                                Supprimer
                            </a>
                        </div>
                        <div>
                            <a href="edit?idPlage=${plage.getId()}">
                                Editer
                            </a>
                        </div>

                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </c:if>
</div>
<%@include file="parts/footer.jsp"%>
</body>
</html>
