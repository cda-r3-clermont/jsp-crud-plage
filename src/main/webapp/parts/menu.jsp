<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 05/09/2023
  Time: 14:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="/crud-plage/">Toutes les plages</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/crud-plage/ajout">Ajouter une plage</a>
        </li>
      </ul>
    </div>
  </div>
</nav>