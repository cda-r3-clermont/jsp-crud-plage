<jsp:useBean id="plage" scope="request" type="com.humanbooster.crudplagedemo.models.Plage"/>
<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 05/09/2023
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>Les plages de la côte !</title>
  <%@include file="parts/script-style.jsp"%>
</head>
<body>
<%@include file="parts/header.jsp"%>
<div class="container">
  <%@include file="parts/menu.jsp"%>
  <h1>Détail de la plage de ${plage.getNom()} !</h1>

    <img src="${plage.getImage()}">

    <div>
      ${plage.number} ${plage.street} ${plage.codePostal} ${plage.city}
    </div>

    <div>
      <label>Contacter le gérant</label>
      <a href="mailto:${plage.getEmail()}">Contacter le gestionnaire !</a>
    </div>
    <a href="/crud-plage/">Revenir sur le listing</a>
</div>
<%@include file="parts/footer.jsp"%>
</body>
</html>
