<%--
  Created by IntelliJ IDEA.
  User: aureliendelorme
  Date: 05/09/2023
  Time: 13:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Edition de la plage ${plageEdit.nom} !</title>
    <%@include file="parts/script-style.jsp"%>
</head>
<body>
<%@include file="parts/header.jsp"%>
<div class="container">
    <%@include file="parts/menu.jsp"%>
    <h1>Edition de la plage ${plageEdit.nom} !</h1>

    <form method="post">
        <div class="form-group col-md-12">
            <label for="nomPlage">Nom de la plage</label>
            <input type="text" value="${plageEdit.nom}" class="form-control" name="plage_nom" placeholder="Nom de la plage" id="nomPlage"/>
        </div>

        <div class="form-group row mt-2">
            <div class="col-md-3">
                <label for="numero">Numero</label>
                <input id="numero" value="${plageEdit.number}" class="form-control" type="text" name="plage_numero" placeholder="Rue"/>
            </div>

            <div class="col-md-3">
                <label for="street">Rue</label>
                <input id="street" value="${plageEdit.street}" class="form-control" type="text" name="plage_street" placeholder="Rue"/>
            </div>

            <div class="col-md-3">
                <label for="codePostal">Code Postal</label>
                <input id="codePostal" value="${plageEdit.codePostal}" class="form-control" type="text" name="plage_codePostal" placeholder="Code Postal"/>
            </div>

            <div class="col-md-3">
                <label for="ville">Ville</label>
                <input id="ville" value="${plageEdit.city}" class="form-control" type="text" name="plage_ville" placeholder="Ville"/>
            </div>

        </div>

        <div class="form-group col-md-12 mt-2">
            <label for="emailGestionnaire">Email du gestionnaire</label>
            <input type="text" value="${plageEdit.email}" class="form-control" name="plage_email" placeholder="Email du gestionnaire" id="emailGestionnaire"/>
        </div>

        <div class="form-group col-md-12 mt-2">
            <label for="photo">Photo</label>
            <input type="text" value="${plageEdit.image}" class="form-control" name="plage_photo" placeholder="URL de la photo de la plage" id="photo"/>
        </div>

        <button type="submit" class="btn btn-success  mt-3">Modifier la plage</button>

        <div class="text-center">
            <c:if test = "${errors.size() !=  0}">
                <ol class="text-danger text-center">
                    <c:forEach items="${ errors }" var="error">
                        <li>${error.message}</li>
                    </c:forEach>
                </ol>
            </c:if>
        </div>
    </form>
</div>


<%@include file="parts/footer.jsp"%>
</body>
</html>
