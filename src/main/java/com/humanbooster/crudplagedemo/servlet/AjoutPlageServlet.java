package com.humanbooster.crudplagedemo.servlet;

import com.humanbooster.crudplagedemo.models.Plage;
import com.humanbooster.crudplagedemo.service.PlageService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import java.io.IOException;
import java.util.Set;

@WebServlet(name = "ajoutPlage", urlPatterns = "/ajout")
public class AjoutPlageServlet extends HttpServlet {

    private PlageService plageService;

    public AjoutPlageServlet() {
        super();
        this.plageService = new PlageService();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("ajout.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Réccupérer tous les éléments dans votre requête
        String nomPlage = request.getParameter("plage_nom");
        String numero = request.getParameter("plage_numero");
        String street = request.getParameter("plage_street");
        String codePostal = request.getParameter("plage_codePostal");
        String ville = request.getParameter("plage_ville");
        String gestionnaire = request.getParameter("plage_email");
        String photo = request.getParameter("plage_photo");

        // Créer un nouvel objet à partir de ces éléments
        Plage plage = new Plage(nomPlage, street, numero, codePostal, ville, gestionnaire, photo);

        // Utiliser la validation pour valider cet objet
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        // Réccupération des erreurs
        Set<ConstraintViolation<Plage>> errors = validator.validate(plage);

        // Il n'y a pas d'erreur, on enregistre et on redirige !
        if(errors.isEmpty()){
            this.plageService.add(plage);
            response.sendRedirect("/crud-plage/");
        } else {
            // Il y a des erreurs, on ajoute dans notre requête les erreurs
            request.setAttribute("errors", errors);
            // On enregistre dans notre requête les saisie utilisateurs pour préremplir nos champs
            request.setAttribute("saisie", plage);
            request.getRequestDispatcher("ajout.jsp").forward(request, response);
        }

    }
}
