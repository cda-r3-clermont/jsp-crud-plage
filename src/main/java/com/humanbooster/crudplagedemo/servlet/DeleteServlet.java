package com.humanbooster.crudplagedemo.servlet;

import com.humanbooster.crudplagedemo.models.Plage;
import com.humanbooster.crudplagedemo.service.PlageService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "deleteServlet", urlPatterns = "/delete")
public class DeleteServlet extends HttpServlet {

    private PlageService plageService;

    public DeleteServlet(){
        super();
        this.plageService = new PlageService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("idPlage"));

        Plage plage = this.plageService.getOne(id);

        if(plage == null){
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else {
            this.plageService.removePlage(plage);
            resp.sendRedirect("/crud-plage/");
        }

    }
}
