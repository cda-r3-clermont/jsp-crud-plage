package com.humanbooster.crudplagedemo.service;

import com.humanbooster.crudplagedemo.models.Plage;
import com.humanbooster.crudplagedemo.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class PlageService {
    public List<Plage> getAll(){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        List<Plage> plages = session.createQuery("FROM Plage", Plage.class).getResultList();

        tx.commit();
        session.close();

        return plages;
    }

    public void add(Plage plage){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        session.persist(plage);

        tx.commit();
        session.close();
    }

    public void update(Plage plage){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        session.merge(plage);

        tx.commit();
        session.close();
    }

    public void removePlage(Plage plage){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        session.remove(plage);

        tx.commit();
        session.close();

    }

    public Plage getOne(long id){
        SessionFactory sf = HibernateUtils.getSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        Plage plage = session.get(Plage.class, id);

        tx.commit();
        session.close();

        return plage;
    }


}
